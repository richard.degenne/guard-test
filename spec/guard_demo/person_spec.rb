# frozen_string_literal: true

describe GuardDemo::Person do
  subject(:person) { described_class.new(age) }

  let(:age) { 28 }

  describe '#major?' do
    subject(:major?) { person.major? }

    it { is_expected.to be true }

    context 'when age is under 18' do
      let(:age) { 17 }

      it { is_expected.to be false }
    end

    context 'when age is 18' do
      let(:age) { 18 }

      it { is_expected.to be true }
    end
  end
end
