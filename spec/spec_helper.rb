# frozen_string_literal: true

require 'rspec'

$LOAD_PATH.unshift(File.join(__dir__, '../lib'))
require 'guard_demo'

RSpec.configure do |config|
  config.order = :rand
end
