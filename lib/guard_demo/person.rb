# frozen_string_literal: true

module GuardDemo
  class Person
    ##
    # @param [Integer] age
    def initialize(age)
      @age = age
    end

    ##
    # @return [Person]
    attr_reader :age

    ##
    # @return [Boolean]
    def major?
      age >= 18
    end
  end
end
